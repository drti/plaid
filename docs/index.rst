.. plaid documentation master file, created by
   sphinx-quickstart
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PLAID documentation
===================

PLAID (Physics Learning AI Datamodel) is a library proposing an implementation for a datamodel tailored for AI and ML learning of physics problems.
It has been developped at SafranTech, the research center of `Safran group <https://www.safran-group.com/>`_

The code is hosted on `Safran Gitlab <https://gitlab.com/drti/plaid>`_

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Getting Started

   source/getting_started.md
   source/description.md

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Advanced

   source/contributing.md

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: API Documentation

   Autoapi <autoapi/plaid/index>
   Basic examples <source/notebooks.rst>
   Convert data into PLAID <source/notebooks/convert_users_data_into_plaid>
   Default values flowchart <source/default_values.rst>

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Going further

   CGNS standard <http://cgns.github.io/>
   Data Challenges <source/data_challenges.rst>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
